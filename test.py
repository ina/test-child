import os

import requests

URL = f'{os.environ["CI_API_V4_URL"]}/projects/{os.environ["CI_PROJECT_ID"]}'


def request(method, endpoint, data=None):
    data = data or {}
    data.update({'token': os.environ['CI_JOB_TOKEN']})
    result = getattr(requests, method)(URL + endpoint, data)
    result.raise_for_status()
    return result.json()


triggered = request('post', '/trigger/pipeline', {'ref': 'master', 'variables[CHILD]': 'true'})
generated_pipeline = request('get', f'/pipelines/{triggered["id"]}')
print(generated_pipeline)
